package transport

import (
	"context"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
	"net/http"
)

func MakeHttpHandler(endpoint *Endpoints) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(NewErrorHandler("http")),
		kithttp.ServerErrorEncoder(encodeError),
		kithttp.ServerBefore(kithttp.PopulateRequestContext),
	}

	pingHandler := kithttp.NewServer(
		endpoint.Ping,
		noDecodeRequest,
		encodeResponse,
		opts...,
	)

	healthHandler := kithttp.NewServer(
		endpoint.Health,
		noDecodeRequest,
		encodeResponse,
		opts...,
	)

	loginHandler := kithttp.NewServer(
		endpoint.Login,
		noDecodeRequest,
		redirectResponse,
		opts...,
	)

	callbackHandler := kithttp.NewServer(
		endpoint.Callback,
		decodeCallbackRequest,
		encodeResponse,
		opts...,
	)

	refreshHandler := kithttp.NewServer(
		endpoint.Refresh,
		decodeRefreshRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/api/v1/ping", pingHandler).Methods("GET")
	r.Handle("/api/v1/health", healthHandler).Methods("GET")
	r.Handle("/api/v1/login", loginHandler).Methods("GET")
	r.Handle("/api/v1/callback", callbackHandler)
	r.Handle("/api/v1/refresh", refreshHandler).Methods("POST")

	return r
}

func noDecodeRequest(context.Context, *http.Request) (interface{}, error) {
	return nil, nil
}

func decodeCallbackRequest(_ context.Context, req *http.Request) (request interface{}, err error) {
	callbackReq := v1.CallbackRequest{
		State: req.FormValue("state"),
		Code:  req.FormValue("code"),
	}

	return callbackReq, nil
}

func decodeRefreshRequest(_ context.Context, req *http.Request) (request interface{}, err error) {
	refreshReq := v1.RefreshRequest{}
	if err = json.NewDecoder(req.Body).Decode(&refreshReq); err != nil {
		return nil, err
	}
	return refreshReq, nil
}

type errorer interface {
	error() error
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func redirectResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}

	if redirectUrl, ok := response.(string); ok && redirectUrl != "" {
		http.Redirect(w, &http.Request{}, redirectUrl, http.StatusTemporaryRedirect)
	}
	return nil
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
	if err != nil {
		log.Error().Err(err).Msgf("Encode to json failed in encodeError")
	}
}
