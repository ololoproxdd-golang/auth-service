package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/service/auth"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/service/health"
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
)

type Endpoints struct {
	Ping     endpoint.Endpoint
	Health   endpoint.Endpoint
	Login    endpoint.Endpoint
	Callback endpoint.Endpoint
	Refresh  endpoint.Endpoint
}

func NewEndpoints(s auth.AuthService, c health.HealthCheck) *Endpoints {
	return &Endpoints{
		Ping:     makePingEndpoint(c),
		Health:   makeHealthEndpoint(c),
		Login:    makeLoginEndpoint(s),
		Callback: makeCallbackEndpoint(s),
		Refresh:  makeRefreshEndpoint(s),
	}
}

func makePingEndpoint(c health.HealthCheck) endpoint.Endpoint {
	return func(context.Context, interface{}) (interface{}, error) {
		return c.Ping()
	}
}

func makeHealthEndpoint(c health.HealthCheck) endpoint.Endpoint {
	return func(context.Context, interface{}) (interface{}, error) {
		return c.Health()
	}
}

func makeLoginEndpoint(s auth.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		return s.Login()
	}
}

func makeCallbackEndpoint(s auth.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(v1.CallbackRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeCallbackEndpoint not type of v1.CallbackRequest")
		}

		return s.Callback(req)
	}
}

func makeRefreshEndpoint(s auth.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(v1.RefreshRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeRefreshEndpoint not type of v1.RefreshRequest")
		}

		return s.Refresh(req)
	}
}
