package transport

import (
	"context"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/rs/zerolog/log"
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
)

type GRPCServer struct {
	ping     kitgrpc.Handler
	health   kitgrpc.Handler
	login    kitgrpc.Handler
	callback kitgrpc.Handler
	refresh  kitgrpc.Handler
}

func MakeGrpcServer(endpoints *Endpoints) v1.AuthServiceServer {
	opts := []kitgrpc.ServerOption{
		kitgrpc.ServerErrorHandler(NewErrorHandler("grpc")),
	}

	return &GRPCServer{
		ping: kitgrpc.NewServer(
			endpoints.Ping,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		health: kitgrpc.NewServer(
			endpoints.Health,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		login: kitgrpc.NewServer(
			endpoints.Login,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		refresh: kitgrpc.NewServer(
			endpoints.Refresh,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		callback: kitgrpc.NewServer(
			endpoints.Callback,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
	}
}

// function not encoding
func noEncodeDecode(_ context.Context, r interface{}) (interface{}, error) {
	return r, nil
}

func (s *GRPCServer) Ping(ctx context.Context, req *empty.Empty) (*v1.PingResponse, error) {
	_, resp, err := s.ping.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}
	res, ok := resp.(v1.PingResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not v1.PingResponse type")
	}

	return &res, nil
}

func (s *GRPCServer) Health(ctx context.Context, req *empty.Empty) (*v1.HealthResponse, error) {
	_, resp, err := s.health.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}
	res, ok := resp.(v1.HealthResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not v1.HealthResponse type")
	}

	return &res, nil
}

func (s *GRPCServer) Login(ctx context.Context, req *empty.Empty) (*v1.LoginResponse, error) {
	_, resp, err := s.login.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}
	res, ok := resp.(*v1.LoginResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.LoginResponse type")
	}

	return res, nil
}

func (s *GRPCServer) Callback(ctx context.Context, req *v1.CallbackRequest) (*v1.AuthResponse, error) {
	_, resp, err := s.callback.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}
	res, ok := resp.(*v1.AuthResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.AuthResponse type")
	}

	return res, nil
}

func (s *GRPCServer) Refresh(ctx context.Context, req *v1.RefreshRequest) (*v1.AuthResponse, error) {
	_, resp, err := s.refresh.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}
	res, ok := resp.(*v1.AuthResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.AuthResponse type")
	}

	return res, nil
}
