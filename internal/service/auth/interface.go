package auth

import (
	"github.com/pkg/errors"
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
	"golang.org/x/oauth2"
)

var (
	accessType       = oauth2.AccessTypeOffline
	stateNotEqualErr = errors.New("State in callback request not equal server state")
	codeExchangeErr  = errors.New("Code exchange failed")
)

type AuthService interface {
	Login() (*v1.LoginResponse, error)
	Callback(v1.CallbackRequest) (*v1.AuthResponse, error)
	Refresh(v1.RefreshRequest) (*v1.AuthResponse, error)
}
