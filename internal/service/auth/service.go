package auth

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/config"
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"net/http"
	"net/url"
)

type authService struct {
	cfg       config.Config
	oauth2Cfg *oauth2.Config
}

type userInfo struct {
	Email   string `json:"email"`
	Company string `json:"company"`
}

type refreshInfo struct {
	AccessToken string `json:"access_token"`
}

func NewAuthService(cfg config.Config) *authService {
	return &authService{
		cfg: cfg,
		oauth2Cfg: &oauth2.Config{
			RedirectURL:  cfg.GoogleRedirectURI,
			ClientID:     cfg.GoogleClientID,
			ClientSecret: cfg.GoogleClientSecret,
			Scopes:       []string{cfg.GoogleScope},
			Endpoint:     google.Endpoint,
		},
	}
}

func (s *authService) Login() (*v1.LoginResponse, error) {
	//TODO https://golang.org/pkg/encoding/json/#Encoder.SetEscapeHTML
	return &v1.LoginResponse{
		RedirectUrl: s.oauth2Cfg.AuthCodeURL(s.cfg.GoogleState, accessType),
	}, nil
}

func (s *authService) Callback(req v1.CallbackRequest) (*v1.AuthResponse, error) {
	if req.State != s.cfg.GoogleState {
		return nil, stateNotEqualErr
	}

	token, err := s.oauth2Cfg.Exchange(oauth2.NoContext, req.Code, oauth2.AccessTypeOffline)
	if err != nil {
		return nil, codeExchangeErr
	}

	userInfo, err := s.getUserInfo(token.AccessToken)
	if err != nil {
		return nil, err
	}

	return &v1.AuthResponse{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
		Email:        userInfo.Email,
		Company:      userInfo.Company,
	}, nil
}

func (s *authService) Refresh(req v1.RefreshRequest) (*v1.AuthResponse, error) {
	values := url.Values{}
	values.Add("grant_type", "refresh_token")
	values.Add("client_id", s.oauth2Cfg.ClientID)
	values.Add("client_secret", s.oauth2Cfg.ClientSecret)
	values.Add("refresh_token", req.RefreshToken)

	response, err := http.PostForm(s.cfg.GoogleRefreshURL, values)
	if err != nil || response.StatusCode >= 300 || response.StatusCode < 200 {
		return nil, fmt.Errorf("failed refreshing tokens: %s", err)
	}

	info := &refreshInfo{}
	if err = json.NewDecoder(response.Body).Decode(&info); err != nil {
		return nil, fmt.Errorf("failed decode refresh info response: %s", err)
	}

	userInfo, err := s.getUserInfo(info.AccessToken)
	if err != nil {
		return nil, err
	}

	return &v1.AuthResponse{
		AccessToken:  info.AccessToken,
		RefreshToken: req.RefreshToken,
		Email:        userInfo.Email,
		Company:      userInfo.Company,
	}, nil
}

func (s *authService) getUserInfo(accessToken string) (*userInfo, error) {
	response, err := http.Get(fmt.Sprintf("%s?access_token=%s", s.cfg.GoogleUserInfoURL, accessToken))
	if err != nil || response.StatusCode >= 300 || response.StatusCode < 200 {
		return nil, fmt.Errorf("failed getting user info: %s", err)
	}

	info := &userInfo{}
	if err = json.NewDecoder(response.Body).Decode(&info); err != nil {
		return nil, fmt.Errorf("failed decode user info response: %s", err)
	}

	return info, nil
}
