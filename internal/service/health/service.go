package health

import (
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
)

type health struct {
}

func NewHealthService() *health {
	return &health{}
}

func (h health) Ping() (v1.PingResponse, error) {
	return v1.PingResponse{Message: "Pong"}, nil
}

func (h health) Health() (v1.HealthResponse, error) {
	return v1.HealthResponse{Message: "Status: Ok"}, nil
}
