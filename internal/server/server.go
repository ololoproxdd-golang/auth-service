package server

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/config"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/service/auth"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/transport"
	"google.golang.org/grpc"
	"net/http"
	"sync"
)

type server struct {
	httpServer *http.Server
	grpcServer *grpc.Server

	config        config.Config
	endpoints     *transport.Endpoints
	healthService health.HealthCheck
	authService   auth.AuthService
}

func NewServer(config config.Config) *server {
	return &server{config: config}
}

// close all descriptors
func (s *server) Close() {
	if err := s.httpServer.Close(); err != nil {
		log.Warn().Err(err).Msg("closing http server")
	}

	s.grpcServer.Stop()
}

// open init structure
func (s *server) Open() error {
	s.initLogger()
	s.registerOsSignal()

	return nil
}

func (s *server) Run() {
	wg := sync.WaitGroup{}
	wg.Add(1)
	go s.runHttpServer(&wg)

	wg.Add(1)
	go s.runGrpcServer(&wg)

	wg.Wait()
	log.Info().Msg("auth-service stopped...")
}
