package server

import (
	"github.com/rs/zerolog/log"
	"net"
	"sync"
)

func (s *server) runGrpcServer(wait *sync.WaitGroup) {
	defer wait.Done()
	address := s.config.AppHost + ":" + s.config.AppPort

	listen, err := net.Listen("tcp", address)
	if err != nil {
		log.Panic().Err(err).Msg("create tcp listener")
	}

	srv := s.getTransportGrpc()
	log.Info().Msgf("running grpc server address: %s", address)

	err = srv.Serve(listen)
	log.Info().Err(err).Msg("closed grpc server")
}

func (s *server) runHttpServer(wait *sync.WaitGroup) {
	defer wait.Done()
	srv := s.getTransportHttp()
	log.Info().Msgf("running http server address: %s", srv.Addr)
	err := s.httpServer.ListenAndServe()
	log.Info().Err(err).Msg("closed http server")
}
