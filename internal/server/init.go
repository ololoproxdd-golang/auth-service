package server

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/service/auth"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/transport"
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	golog "log"
	"net/http"
	"os"
	"time"
)

func (s *server) getAuthService() auth.AuthService {
	if s.authService != nil {
		return s.authService
	}
	s.authService = auth.NewAuthService(s.config)
	log.Info().Msgf("created auth service")

	return s.authService
}

func (s *server) getHealthService() health.HealthCheck {
	if s.healthService != nil {
		return s.healthService
	}
	s.healthService = health.NewHealthService()
	log.Info().Msgf("created health service")

	return s.healthService
}

func (s *server) getEndpoints() *transport.Endpoints {
	if s.endpoints != nil {
		return s.endpoints
	}
	s.endpoints = transport.NewEndpoints(s.getAuthService(), s.getHealthService())
	log.Info().Msgf("created server endpoints")
	return s.endpoints

}

func (s *server) getTransportGrpc() *grpc.Server {
	if s.grpcServer != nil {
		return s.grpcServer
	}
	s.grpcServer = grpc.NewServer(grpc.UnaryInterceptor(GrpcLoggerInterceptor))
	v1.RegisterAuthServiceServer(s.grpcServer, transport.MakeGrpcServer(s.getEndpoints()))

	// Register reflection service on gRPC server.
	reflection.Register(s.grpcServer)

	log.Info().Msgf("created transport - grpc")

	return s.grpcServer
}

func (s *server) getTransportHttp() *http.Server {
	if s.httpServer != nil {
		return s.httpServer
	}
	s.httpServer = &http.Server{
		Handler:      transport.MakeHttpHandler(s.getEndpoints()),
		Addr:         fmt.Sprintf("%s:%s", s.config.AppHost, s.config.AppHttpPort),
		ErrorLog:     golog.New(os.Stdout, "http: ", golog.LstdFlags),
		WriteTimeout: 2 * time.Second,
		ReadTimeout:  2 * time.Second,
	}

	log.Info().Msgf("create transport - http")

	return s.httpServer
}
