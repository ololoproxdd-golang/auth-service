package main

import (
	"gitlab.com/ololoproxdd-golang/auth-service/internal/config"
	"gitlab.com/ololoproxdd-golang/auth-service/internal/server"
	"os"
)

func main() {
	appServer := server.NewServer(config.LoadConfig())
	if err := appServer.Open(); err != nil {
		os.Exit(1)
		return
	}

	appServer.Run()
}
